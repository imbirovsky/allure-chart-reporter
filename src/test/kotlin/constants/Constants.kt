package constants

class Constants {

    object API {
        const val HTTPS = "https://"
        const val URL = "restcountries.com/"
        const val VERSION_API = "v3.1/"
        const val ACTION_NAME = "name/"
    }

    object Countries {
        const val UKRAINE = "ukraine"
        const val USA = "usa"
        const val GERMANY = "germany"
        const val FRANCE = "france"
        const val ITALY = "italy"
        const val SPAIN = "spain"
        const val POLAND = "poland"
        const val RUSSIA_IS_A_TERRORIST_STATE = "country_404"
    }

}